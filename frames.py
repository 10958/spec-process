import numpy as np
from astropy.io import fits
from astropy.table import Table
import sys
try:
    import imageio.v2 as imageio
except ModuleNotFoundError:
    sys.exit("'imageio' package is required. `pip install imageio`")
from matplotlib import pyplot as plt
import os
from os import getcwd, listdir, mkdir
from time import time
from scipy import ndimage
try:
    import scienceplots
    plt.style.use("science")
except ModuleNotFoundError:
    pass


current_dir = getcwd()

def find_files(dir: str=current_dir, include_terms: list[str]=[], exclude_terms: list[str]=[]) -> list[str]:
    return sorted(
        [file for file in listdir(dir) if all(iterm.lower() in (lower_file:=file.lower()) for iterm in include_terms) and not any(eterm.lower() in lower_file for eterm in exclude_terms)])


def timer(func, *args, **kwargs):
    def wrapper(*args, **kwargs):
        t1 = time()
        out = func(*args, **kwargs)
        print(f"{func.__name__!r} took {time()-t1:.4f} s.")
        return out
    return wrapper

@timer
def make_frames(xmin, xmax, ymin, ymax):
    dir_in = current_dir
    files = find_files(dir=dir_in, include_terms=[".fit"])
    dir_out = os.path.join(dir_in, "png")
    if not os.path.isdir(dir_out):
        mkdir(dir_out)

    for file in files:
        path = os.path.join(dir_in, file)

        with fits.open(path) as hdul:
            try:
                print(f"{path}")
                table = Table.read(hdul, hdu=1)
                wavelen = table["lambda"]
                data_ax = table["flattened"]

                image = hdul[0].data
                img_head = hdul[0].header

                fig, axs = plt.subplot_mosaic([["graph"],["image"]], height_ratios=([4, 1]))#, layout="constrained")
                axs["graph"].plot(wavelen, ndimage.median_filter(data_ax, 5), color="k")
                axs["graph"].set_ylabel(r"Relative Intensity")
                axs["image"].set_xlabel(r"Wavelength $(\mathrm{\AA})$")
                axs["graph"].set_xlim((xmin,xmax))
                axs["graph"].set_ylim((ymin,ymax))

                axs["image"].imshow(image, origin="lower", extent=[img_head["B0"], (img_head["M0"]*image.shape[1] + img_head["B0"])/(1 - img_head["C1"]*image.shape[1]), 0, image.shape[0]], aspect="auto", cmap="grey")
                # axs["image"].imshow(image, origin="lower", extent=[img_head["CRVAL1"], img_head["CRVAL1"] + img_head["CDELT1"]*image.shape[1], 0, image.shape[0]], aspect="auto", cmap="grey")
                axs["image"].sharex(axs["graph"])
                axs["image"].set_yticklabels = []
                plt.subplots_adjust(wspace=0, hspace=0)
                fig.set_size_inches(7,7)
                axs["graph"].set_title(f"Beta Aurigae: {os.path.basename(path).split('_')[0]}")
                plt.tight_layout()
                plt.savefig(os.path.join(dir_out, f"frame_{file}.png"))
                # plt.show()
                plt.close()
            except KeyboardInterrupt:
                break
            except KeyError as kerr:
                print(f"Key Error in {path}. Was this file processed?")

@timer
def make_gif():
    directory = os.path.join(current_dir, "png")
    files = find_files(dir=directory, include_terms=[".png"])

    join_char = os.path.join("a", "a").replace("a", "")
    splits = current_dir.split(join_char)
    print(splits)
    folder_name = splits[-3]

    import imageio
    with imageio.get_writer(os.path.join(directory, f"{folder_name}.gif"), mode="I", loop=0) as writer:
        for filename in files:
            print(f"gif {(full_path := os.path.join(directory, filename))}")
            image = imageio.imread(full_path)
            writer.append_data(image)


# Set this to the directory containing the 1D spectra
current_dir = ""

## make_frames(start wavelength, end wavelength, low flux, high flux)
# make_frames(6475,6660, 0.2,1.1)

# make_gif()