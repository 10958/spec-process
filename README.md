# SpecProcess
## Automated Astronomical Spectrum Processing Utilities
---
SpecProcess (`process2.py`) contains tools for processing astronomical spectra. Included processing tools are:
- [x] Image reduction
- [x] Optical distortion correction
- [x] Continuum fitting
- [x] Wavelength calibration (Automation WIP)
- [x] Flux calibration
- [ ] Peak characterization (Improvements WIP)

---
### Command-Line Interface
Flags:
- [x] `--silent` reduces command-line output to only essential information (opt)
- [x] `--location <path>` specifies the location where files are found (opt: defaults to directory with `py` file)
- [x] `--vertical` tells the program to expect vertical spectra (opt: auto-detection exists)
- [x] `--calibs <path>` specifies a directory containing only calibration images (opt)
- [x] `--spectra <path>` specifies a directory containing only data images (opt)
- [x] `-b <int>` `--blur <int>` specifies a global median blur strength (opt)
- [ ] `--hotpixfix` enables targetted hot pixel fix (WIP)
- [ ] `--sigma_clip <std dev>` enables standard-deviation-based sigma clipping of stacked images/hot pixels (WIP)
- [x] `-l <int>` `--lines <int>` specifies number of lines in calibration spectrum (opt)
- [x] `--period <float>` specifies a system period (opt: sorts by phased period if flagged)
- [x] `--epoch <float>` specifies Julian epoch for a phased period (opt)
- [ ] `--frame` enables frame generation for GIFs (opt)
- [x] `--prominence <float>` sets prominence (counts) considered to be a peak (opt)
- [x] `--cache` enables caching of repetitive data entry tasks (opt; recommended)
- [x] `--dao` enables [DAO](https://nrc.canada.ca/en/research-development/nrc-facilities/dominion-astrophysical-observatory-research-facility) mode (vertical spectra, preset header information) (opt)
- [ ] `--auto` enables fully-automatic processing (WIP) (opt)
- [x] `--combined_calibs <int>` specifies number of calibration images considered to be a "set" (opt)
- [x] `--artificial_bias <int>` value to add to all pixel values (opt)
- [x] `--rows_sum_inclusion_threshold <int>` Minimal sum of image rows to be considered data (opt)
- [x] `--resolution_limit <float>` specifies the resolution of the spectrograph for wavelength calibration

---
### Web App
*Work in progress*

The web app will have a profile system allowing for easy repeatability and transportability. All parameters will be user-adjustable. Steps where user intervention may be desired will have intervention points. Large file upload support ($\geq10$ GB) is expected.

The web app is under construction using an HTML+JavaScript frontend with a Python/Flask backend. HTTPS will be supported for secure and private file uploads.

***
## Dependencies
* [`os`](https://docs.python.org/3/library/os.html), [`sys`](https://docs.python.org/3/library/sys.html), [`argparse`](https://docs.python.org/3/library/argparse.html), [`warnings`](https://docs.python.org/3/library/warnings.html) OS/CLI Management
* [`typing`](https://docs.python.org/3/library/typing.html) File typing
* [`pathlib`](https://docs.python.org/3/library/pathlib.html) Formatting file paths
* [`shutil`](https://docs.python.org/3/library/shutil.html) Moving/copying files
* [`re`](https://docs.python.org/3/library/re.html), [`csv`](https://docs.python.org/3/library/csv.html) Parsing filenames
* [`timeit`](https://docs.python.org/3/library/timeit.html) Measure run duration(s)
* [`tracemalloc`](https://docs.python.org/3/library/tracemalloc.html) Measure memory usage
* [`astropy`](https://www.astropy.org/) FITS handling
* [`numpy`](https://numpy.org/) Arrays, vectorised operations, etc.
* [`scipy`](https://scipy.org/) Array operations, signal processing
* [`pandas`](https://pandas.pydata.org/) Data IO
* [`matplotlib`](https://matplotlib.org/) Plotting and frame generation
* [`mpl_point_clicker`](https://pypi.org/project/mpl-point-clicker/) Interacting with open `pyplot` instances
* `frames.py`:
  * [`imageio`](https://pypi.org/project/imageio/)
  * [`scienceplots`](https://pypi.org/project/SciencePlots/) (optional; slow)
***
## Defaults
* Directories
  * Sort observations by object. The most recent previous calibration images are used by default. This can be changed within the code.
  * Original spectra are moved to `./SpOriginals` (or `.\\SpOriginals` on Windows) before anything is modified. Backups can be disabled in `class Values`.
* File Naming
  * Spectra contain `Sp` in the filename (parametrised in `class Values`)
  * Calibration images contain `Cal` in the filename (parametrised in `class Values`)
  * Files contain Universal Time Date as "`UT``yyyy``mm``dd`" in the filename (parametrised in `class Values`)
* Caching
  * When caching is enabled by the `--cache` flag, previously-entered wavelength values will be stored and used for the next calibration (if you response `y` to the prompt). If you do not want to apply the cache to the next calibration image, you can answer `n` to the prompt and enter new values.
* System Phase
  * By default, it is assumed that the system has no phase by which to sort observations. Setting `--phase <decimal days>` sorts files in `filemap.csv` by fractional phase (16 decimals) instead of Julian date (default when no phase) from file headers.
***
Checkboxes reflect features currently publicly available.
Built in [Python 3.11.8](https://www.python.org/downloads/release/python-3118/) using CPython. Licensed under [GPL3.0](https://www.gnu.org/licenses/gpl-3.0.en.html).