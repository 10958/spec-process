"""
Spectrum Processing Utility
==============================================================================
Author: Daniel Currie, dc0@tuta.io
Available at: https://codeberg.org/10958/spec-process
License: GPLv3.0+: https://www.gnu.org/licenses/gpl-3.0.en.html
"""
__version__ = 2

# CLI and file management
import os
import sys
import argparse
import re
from pathlib import Path
import shutil

# Resource usage monitoring
from datetime import datetime
import warnings
import timeit as time
import tracemalloc
from typing import Any

# Data management and operations
import numpy as np
from numpy import typing as nptp
import pandas as pd
from scipy import stats, signal, ndimage
from scipy.optimize import curve_fit
import csv
from astropy.io import fits
from astropy.time import Time
from PIL import Image, ImageDraw

# Data visualisation
import matplotlib as mpl
from matplotlib import pyplot as plt
from mpl_point_clicker import clicker


##############################################################################
# Classes

class Values:
    """Stores global values."""

    def __init__(self, mode: str = "OWN", config: str | None = None):
        """
        Initialise values for global access.

        Parameters
        ----------
        mode : str
            - "OWN": own observation defaults
            - "DAO": DAO observation defaults
        """
        self.location: str = os.getcwd()
        self.log_name: str = "console.log"
        self.storage_used: int = 0
        self.n_spectra: int = 0
        self.master_calib_tag: str = "calib_master"
        self.transform_tag: str = "transform"
        self.calib_map_name: str = "calib_map.csv"
        self.filemap_name: str = "filemap.csv"
        self.backup_spectra: bool = True
        self.backup_path: str = "SpOriginal"
        self.overwrite: bool = True
        # Set these default universal values
        # to match your expectations. These are
        # used in all calibrated and master images.
        self.univ_header_path: str = "PATH"
        self.univ_header_ut: str = "UTDATE"
        self.univ_header_jd: str = "JULDATE"  # NOTE: ALSO IN MapHandler._add
        self.univ_header_transform: str = "NEARTRANS"
        self.univ_header_trimCLOW: str = "TRIMCLOW"
        self.univ_header_trimCHI: str = "TRIMCHI"
        self.univ_header_trimRLOW: str = "TRIMRLOW"
        self.univ_header_trimRHI: str = "TRIMRHI"
        self.univ_header_b0: str = "B0"
        self.univ_header_m0: str = "M0"
        self.univ_header_c1: str = "C1"
        self.univ_header_crrow: str = "CRROW1"
        self.univ_header_anchors: str = "CRCOLS"
        self.univ_header_reverse: str = "REVERSE"
        # Set these defaults to match your files
        # or add your own mode and build your own functions.
        if mode == "OWN":
            self.tag_header_UT_date: str = "DATE-OBS"
            self.tag_header_julian_date: str = "JD"
            self.tag_filename_spectrum: str = "Sp"
            self.tag_filename_calib: str = "Cal"
            self.combined_calibs: int = 1
        if mode == "DAO":
            self.tag_header_UT_date: str = "DATE-OBS"
            self.tag_header_julian_date: str = "MJD-OBS"
            self.tag_header_calib: str = "comparison"
            self.combined_calibs: int = 1


class Utilities:
    """Utilities for spectrum processing."""

    @staticmethod
    def find(path: str, *names: tuple, files: list = None,
             search_header: bool = False, mode: str = "equal",
             fill: str = "ààà", **kwargs: dict) -> list:
        """
        Find filenames of files with *args in their filenames AND
        **kwargs in their headers. Note: **kwargs are only applicable
        when search_header=True.

        If FITS files are to be found, look for `.fit` instead of `.fits`.

        If a keyword contains a dash "-", replace it with "ààà" (default).

        Parameters
        ----------
        path : str
            Path to directory to search for filenames
        *names : tuple
            Iterable containing filename attributes to find
        files : list
            Files to search in. If None (default),
            values.location is searched instead.
        search_header : bool
            Whether to search the header of the file
            - Default: False.
            - Compatibility: `FIT(S)` file format
        mode : str
            Mode of checking headers
            - "equal" (default): value is equal to keyword value
            - "in": value is in keyword value
        fill : str
            Value used to fill in for '-' in headers.
            Default 'ààà' replaced by '-'.
        **kwargs : dict
            Keyed arguments used only when search_header=True.
            Headers are searched for these key-value pairs.

        Returns
        -------
        out : list
            List containing filenames as strings

        Example
        -------
        Find FITS files in directory '/home/spec/DAO' with
        key 'OBSTYPE' set to 'bias' in the FITS header.

        `Utilities().find("/home/spec/DAO/", ".fit", search_header=True, OBSTYPE="bias")`

        """
        if files is None:
            all_files = np.array(os.listdir(path))
        else:
            all_files = np.array(files)

        # Files containing contents of *args in filenames
        out = all_files[
            np.prod(
                [
                    np.array([a in f for a in names], dtype=list) for f in all_files
                ], axis=1, dtype=bool)
        ].tolist()
        out = [os.path.join(v.location, f) for f in out]

        # Search headers, adding additional files.
        # To make these the ONLY files, use only **kwargs.
        if search_header:
            # Add tests for different filetypes here to
            # extend compatibility (e.g. for csv headers).
            # Make dictionary { 'filename' : header, ... }
            all_fits = [{os.path.join(v.location, f): fits.getheader(os.path.join(v.location, f))} for f in all_files if
                        ".fit" in f]

            in_header = []
            for f in all_fits:
                if mode == "equal":
                    head_search = [
                        np.prod([
                            val[a.replace(fill, "-")] == t for a, t in kwargs.items()
                        ]) for k, val in f.items()
                    ]
                elif mode == "in":
                    head_search = [
                        np.prod([
                            str(t) in val[str(a).replace(fill, "-")] for a, t in kwargs.items()
                        ]) for k, val in f.items()
                    ]
                else:
                    u.halt(f"Invalid search mode '{mode}'. < equal, in >")

                # Discard empty lists in results
                if head_search[0]:
                    in_header.append(list(f)[0])

            # Keep only file paths which satisfy ALL requirements
            out = list(set(out).intersection(in_header))
        return out

    @staticmethod
    def read(path: str, mode: str) -> pd.DataFrame | tuple[Any, np.ndarray]:
        """
        Read a file from a given path.

        Parameters
        ----------
        path : str
            Path to a file to be read.
        mode : str
            Read mode:
            - "PQ": Read .parquet filetypes
            - "FITS": Read .fit(s) filetypes
            - "CSV": Read .csv filetypes

        Returns
        -------
        data : tuple
            - pd.DataFrame if mode is PQ or CSV
            - 2-tuple (FITS Header, np.ndarray) if mode is FITS

        """
        try:
            # Read parquet file
            if mode == "PQ":
                out = pd.read_parquet(path)
            # Read fits file
            elif mode == "FITS":
                hdul = fits.open(path)
                fits_header = hdul[0].header
                fits_data = hdul[0].data
                hdul.close()
                # No negatives allowed
                fits_data[fits_data<0] = 0
                out = fits_header, fits_data
            # Read csv file
            elif mode == "CSV":
                out = pd.read_csv(path)
            # Otherwise, probably not a valid file type
            else:
                u.halt(f"Error:\tInvalid mode '{mode}'. < PQ | FITS | CSV >")
            return out
        except FileNotFoundError as fnf_err:
            u.halt(f"File error: {fnf_err}")

    @staticmethod
    def move(path: str, dest: str):
        """
        Safely move a file from path to dest.
        If path is in a different partition than dest,
        copy and then delete on success.
        """
        if not os.path.exists(dest):
            os.mkdir(dest)
        shutil.move(path, os.path.join(dest, os.path.basename(path)))

    @staticmethod
    def date(path: str, mode: str = "HEADER", form: str = "DATE-OBS"):
        """
        Extract the date from a file either from its filename or its header.

        Parameters
        ----------
        path : str
            Path to a file to evaluate
        mode : str
            Whether to check the header or the filename
            - "NAME": (Default) check filenames
            - "HEADER": check file headers
        form : str
            Format of date to find, e.g.
            - "UT" to find date in filename tagged with 'UTYYYMMDD'
            - "MJD-OBS" to find Modified Julian date in a FITS header

        """
        if mode == "NAME":
            out = [
                # Check each piece of the filename for 'form'
                d for d in list(re.split(r"[_\-]", path)) if form in d
            ][0].replace(form, "")
        elif mode == "HEADER":
            # Look in FIT(S) header for 'form'
            if path.endswith(".fit") or path.endswith(".fits"):
                out = fits.getheader(path)[form]
            # Else, can't read the header
            else:
                u.halt("File does not contain a compatible header. < FITS >")
        return out

    def get_dates(self, files: list) -> \
            np.ndarray | tuple[np.ndarray, np.ndarray | None, np.ndarray | None, np.ndarray | None]:
        """
        Get unique dates from a list of filenames by searching FITS header.

        Parameters
        ----------
        files : list
            List of files in which to search for dates
        """
        cdates = np.unique(
            [
                Time(
                    self.date(c, mode="HEADER", form=v.tag_header_UT_date
                              ), format="fits", scale="utc", out_subfmt="date"
                ).value for c in files
            ]
        )
        return cdates

    @staticmethod
    def wavelength_calib_equation(xs, m0, b0, c1):
        """Get wavelength output for xs indices

        y = (m0*x + b0)/(1 - c1*x)

        Args:
            xs (np.ndarray): Array of indices
            m0 (float): Modified slope
            b0 (float): Modified intercept
            c1 (float): Original intercept

        Returns:
            np.ndarray: Wavelengths associated with the input parameters and indices
        """
        return ((m0*xs)+b0)/(1-(c1*xs))

    @staticmethod
    def nearest(
            date: float, all_dates: list | np.ndarray,
            mode: str = "subtract", direction: str = "before",
            give: str = "value"
    ) -> float:
        """
        Get nearest date to 'date' in array 'all_dates'.

        Parameters
        ----------
        date : float
            Date to compare
        all_dates : list | np.ndarray
            List or array of dates in which to search
        mode : str
            How to compare dates.
            Default (only): subtract
        direction : str
            Which way to look in 'all_dates'
            - "before" looks only BEFORE date
            - "after" looks only AFTER date
            - "any" looks at everything
        give : str
            Mode of return.
            - "value" returns *value* of nearest date
            - "index" returns *index of nearest date

        Returns
        -------
        out : float
            - Index of nearest date if 'give' is "index".
            - Value of nearest date if 'give' is "value" or otherwise.
        """
        if mode == "subtract":
            arr = np.asarray(all_dates, dtype=float)
            match direction:
                case "before":
                    idx = np.abs(arr[arr < date] - date).argmin()
                case "after":
                    idx = np.abs(arr[arr > date] - date).argmin()
                case "any":
                    idx = np.abs(arr - date).argmin()
                case other:
                    u.halt(f"Invalid date direction '{direction}'. < before | any | after >")

            if give == "index":
                return idx
            return all_dates[idx]
        else:
            u.halt("Invalid date comparison mode.")

    @staticmethod
    def remove_nan_cols(arr: np.ndarray) -> np.ndarray:
        """Remove columns containing ONLY NumPy NaN"""
        return arr[:, ~np.isnan(arr).all(axis=0)]

    @staticmethod
    def median_stack(arrs: list[np.ndarray]) -> np.ndarray:
        """
        Median stack NumPy arrays.

        Parameters
        ----------
        *arrs : list
            Numpy arrays to median stack

        Returns
        -------
        stacked : np.ndarray
            Median stacked array
        """
        if len(arrs) > 1:
            # Not a memory efficient way to stack huge arrays,
            # but doing it line-by-line takes way longer.
            force_type = type(arrs[0][0][0])
            arrays = [np.array(a, dtype=force_type) for a in arrs]
            stacked = np.median(np.array(arrays), axis=0, overwrite_input=True)
            del arrs, arrays
            stacked = stacked.astype(force_type)
        else:
            stacked = arrs[0]
        return stacked
    
    @staticmethod
    def smoothen(data, box_size):
        """Smooth a dataset using a moving average while respecting edges
        Credit to Turun: https://stackoverflow.com/users/14053391/turun

        Args:
            data (nptp.NDArray): 1-D signal to be smoothed
            box_size (int): Size of moving average box

        Returns:
            nptp.NDArray: Smoothed 1-D signal
        """
        data_sum = np.cumsum(data)
        moving_average = (data_sum[2*box_size:] - data_sum[:-2*box_size]) / (2*box_size)
        
        for i in range(1, box_size):
            (first := [np.average(data[:box_size])]).append(np.average(data[:i + box_size]))
            (last := []).insert(0, np.average(data[-i - box_size:]))
        last.insert(0, np.average(data[-2 * box_size:]))
        return np.concatenate((first, moving_average, last))

    @staticmethod
    def sout_header(text: str, fill: str = "=", force: bool = False) -> None:
        """
        Print a 72-char wide line with `text` centered.

        Parameters
        ----------
        text : str
            Text to be printed in full line
        fill : str
            String value used to fill space in the header line
        force : bool
            Whether to force printing of the header even with verbosity off

        Examples
        -------
        >>> >>> u.header_line("Even")
        '========[ Even ]========'
        >>> >>> u.header_line("Odd")
        '========[ Odd ]========='
        """
        L_fill = (-2 + (72 - len(text)) // 2)
        R_fill = ((72 - len(text)) // 2) - (1 if len(text) % 2 == 1 else 2)
        line = f"{fill * L_fill}[ {text} ]{fill * R_fill}"
        L.log(line)
        if force or not args.silent:
            print(line)
        else:
            pass

    @staticmethod
    def sout(text: str, end: str = "\n", force: bool = False) -> None:
        """
        Print a single line containing with `text`

        Parameters
        ----------
        text : str
            Text to be printed on a line
        end : str
            Line ending (e.g. '', \\n, \\r, etc.)
            Default: "\n"
        force : bool
            Whether to force printing of the line even with verbosity off
        """
        L.log(text)
        if force or not args.silent:
            print(text, end=end)
        else:
            pass

    @staticmethod
    def sinput(message: str) -> Any:
        """
        Get input from console.

        Parameters
        ----------
        message : str
            Message printed to console
        result : Any
            What is input into the console

        Returns
        -------
        result : Any
            Input from console
        """
        L.log(message)
        result = input(message)
        L.log(result)
        return result

    @staticmethod
    def splot(data: np.ndarray, mode: str = "sum", ptile: int = 90, n_spaces: int = 5) -> None:
        """
        Plot a 72-char wide plot in the console
        """
        if mode == "sum":
            oned = np.sum(data, axis=0)
        if mode == "average":
            oned = np.average(data, axis=0)

        p = np.percentile(oned, ptile)
        n = len(oned)
        markers = np.linspace(0, n, n_spaces, dtype=int)
        s = "".join("|" if i > p else "_" for i in oned)[::n // 72]
        if len(s) > 72:
            s = s[len(s) - 72:]
        spacing = (len(s) - np.sum([len(str(x)) for x in markers], dtype=int)) // (n_spaces - 1)
        s2 = f"{' ' * spacing}".join(str(x) for x in markers)
        L.log(s)
        L.log(s2)
        if not args.silent:
            print(s)
            print(s2)

    @staticmethod
    def halt(text: str) -> None:
        """
        Halt the program with sys.exit, printing 'text'.

        Parameters
        ----------
        text : str
            Text to log and print when process is halted.
        """
        L.log(text)
        sys.exit(text)


class Log:
    """Log file which records all potential console output"""

    def __init__(self):
        if v.location:
            self.loc = os.path.join(v.location, v.log_name)
        else:
            print("You should initialise Values() before logging.")
            self.loc = os.path.join(os.getcwd(), "report.log")
        if not os.path.exists(self.loc):
            open(self.loc, "w").close()

    def clear(self):
        """Clear contents of existing log file."""
        if os.path.exists(self.loc):
            open(self.loc, "w").close()

    def log(self, text: str) -> None:
        with open(self.loc, "a") as f:
            f.write(f"{text}\n")


class CalibMap:
    """A map of calibration images and their properties"""

    def __init__(self, path: str) -> None:
        self.path = path
        if not os.path.exists(self.path):
            open(self.path, "w").close()

    def clear(self):
        """Clear contents of existing calibration image map."""
        if os.path.exists(self.path):
            open(self.path, "w").close()

    def write(self, *data: Any) -> None:
        """
        Write data to a calibration image map.
        If the file does not exist, create one.

        Parameters
        ----------
        data : Any
            Row to be written to the calibration map
        """
        with open(self.path, "a") as f:
            writer = csv.writer(f, delimiter=",")
            writer.writerow(*data)

class Calib:
    """Calibration image"""

    def __init__(self, path: str):
        self.filename = path
        self.calib = fits.getdata(self.filename, memmap=False)
        self.header = fits.getheader(self.filename, memmap=False)
        self.rows, self.cols = self.calib.shape
        # Values used for trimming calibration spectrum
        # L, R, T, B in reference to how an image normally
        # appears using PyPlot's imshow().
        self.TRIML = None
        self.TRIMR = None
        self.TRIMT = None
        self.TRIMB = None
        # Values used for wavelength and shape finding
        self.direction = None
        self.anchor_row = 0
        self.anchors = None
        self.offsets = None
        # Whether spectrum is backwards
        self.reverse = False

    def get_corners_manual(self) -> dict:
        """Get corners of calibration spectrum from user"""
        fig, ax = plt.subplots(tight_layout=True)
        vmin = np.percentile(self.calib, 15)
        vmax = np.percentile(self.calib, 90)
        ax.imshow(self.calib, cmap="gray", aspect="auto", zorder=0, vmin=vmin, vmax=vmax)
        clk = clicker(ax, ["corner"], markers=["s"], linestyle="-", colors=["red"], zorder=2)

        def __draw_ax_line(position, label):
            x, y = position
            plt.axhline(y=y, linestyle="--", zorder=1)
            plt.axvline(x=x, linestyle="--", zorder=1)

        clk.on_point_added(__draw_ax_line)
        ax.get_legend().remove()
        plt.title(
            "Adjust contrast as necessary. Left click to mark corners.\
                \nDo not pan or zoom!\
                \nClose window when all four corners are marked.")
        plt.xlabel(f"{self.filename}")
        u.sout("Mark the corners of the calibration spectrum.", end="\r")
        plt.show()
        corners = clk.get_positions()["corner"]
        plt.clf()
        plt.close("all")
        if len(corners) != 4:
            # TODO: Add automatic edge finding and run that here with a warning?
            u.sout(" " * 80, end="\r")
            u.sout(f"Error:\tMust define exactly 4 corners in file '{Path(self.filename).name}'.\n\tYou defined {len(corners)}.")
            user_in = u.sinput("Use 0 as the border size on every edge? (Y/N): ")
            if user_in.upper() == "Y":
                corners = np.array([
                    [0, 0],
                    [0, self.rows],
                    [self.cols, 0],
                    [self.cols, self.rows]
                ])
            else:
                u.halt("Error:\tEdge positions must be defined.")
        u.sout(f" " * 80, end="\r")
        u.sout(f"Corners marked for '{Path(self.filename).name}'.")
        return corners

    def set_trims(self, corners: dict) -> None:
        edges = {
            "img_TL": np.array([0, 0]),
            "img_BL": np.array([0, self.rows]),
            "img_TR": np.array([self.cols, 0]),
            "img_BR": np.array([self.cols, self.rows])
        }

        # Determine the order of corners as they were selected.
        order = {
            list(edges.keys())[
                np.array([
                    np.sum(np.abs(edges[e] - cn)) for e in edges.keys()
                ], dtype=list).argmin()
            ]: cn.astype(int) for cn in corners
        }

        # Get offsets from corner positions and image edges
        self.TRIML = max(order["img_TL"][0], order["img_BL"][0])
        self.TRIMR = self.cols - min(order["img_TR"][0], order["img_BR"][0])
        self.TRIMT = max(order["img_TL"][1], order["img_TR"][1])
        self.TRIMB = self.rows - min(order["img_BL"][1], order["img_BR"][1])

        if self.TRIML == 0:
            self.TRIML = 1
        if self.TRIMR == 0:
            self.TRIMR = 1
        if self.TRIMT == 0:
            self.TRIMT = 1
        if self.TRIMB == 0:
            self.TRIMB = 1

    def trim(self, overwrite: bool = False) -> dict:
        """
        Apply trim to master calibration image.
        Returns trimmed (array, (top trim, bottom trim))

        Parameters
        ----------
        overwrite : bool
            Whether to overwrite existing FIT(S) image once trimmed.
            If 'False', no new file is written.

        Returns
        -------
        out : dict
            Dictionary containing calibration image and trims
        """
        Z = np.zeros_like(self.calib)
        i = np.ogrid[self.TRIMT:self.rows - self.TRIMB, self.TRIML:self.cols - self.TRIMR]
        Z[i[0], i[1]] = 1

        # Blur if requested to remove some noise
        if args.blur != 0:
            self.calib = ndimage.median_filter(self.calib, size=args.blur)

        if overwrite:
            self.header.append((v.univ_header_trimCLOW, self.TRIML, "Number of px trimmed from low column indices"))
            self.header.append((v.univ_header_trimCHI, self.TRIMR, "Number of px trimmed from high column indices"))
            self.header.append((v.univ_header_trimRLOW, self.TRIMT, "Number of px trimmed from low row indices"))
            self.header.append((v.univ_header_trimRHI, self.TRIMB, "Number of px trimmed from high row indices"))
            self.calib = self.calib * Z
            if os.path.exists(self.filename):
                size_before = os.path.getsize(self.filename)
            else:
                size_before = 0
            fits.writeto(self.filename, self.calib, header=self.header, overwrite=True)
            v.storage_used += os.path.getsize(self.filename) - size_before
            return {"calib": self.calib, "trims": {"trim_L": self.TRIML, "trim_R": self.TRIMR,
                                                   "trim_T": self.TRIMT, "trim_B": self.TRIMB}}
        else:
            return {"calib": self.calib * Z, "trims": {"trim_L": self.TRIML, "trim_R": self.TRIMR,
                                                       "trim_T": self.TRIMT, "trim_B": self.TRIMB}}

    def find_anchors(self) -> None:
        """
        Find the anchor row and anchor columns in the image.
        If the brightest end of a line is in the top half of
        an image, choose the top of the lines as the anchor.
        Otherwise, choose the bottom of the lines.
        """
        # Get trims if not already present
        if (self.TRIML is None) or (self.TRIMR is None) or (self.TRIMT is None) or (self.TRIMB is None):
            self.TRIML = self.header[v.univ_header_trimCLOW]
            self.TRIMR = self.header[v.univ_header_trimCHI]
            self.TRIMT = self.header[v.univ_header_trimRLOW]
            self.TRIMB = self.header[v.univ_header_trimRHI]

        # Index of row with maximal value
        max_index = self.calib.sum(axis=1).argmax()
        if self.anchor_row is None:
            # If max index is below middle of image, anchor to bottom
            # Note that NumPy has (0,0) in top left
            # Direction in reference to PyPlot's imshow()
            if max_index > (self.rows - self.TRIMT) / 2:
                self.anchor_row = self.rows - self.TRIMB - 1
            else:
                self.anchor_row = self.TRIMT
        elif max_index > (self.rows - self.TRIMT) / 2:
            self.anchor_row = self.rows - self.TRIMB - 1
        else:
            self.anchor_row = self.TRIMT

        # Get anchors
        anchors, _ = signal.find_peaks(self.calib[self.anchor_row], prominence=args.prominence)
        # Anchor and value
        unsorted_anchors = np.array([
            [anchor, self.calib[self.anchor_row, anchor]] for anchor in anchors
        ])
        # Sorted by value
        sorted_anchors = unsorted_anchors[unsorted_anchors[:, 1].argsort()[::-1]][:args.lines]
        # Sorted by value then now by index
        self.anchors = sorted_anchors[sorted_anchors[:, 0].argsort()].tolist()
        # self.anchors = dict(zip(anchors, self.calib[self.anchor_row][anchors]))

    def get_wavelengths(self, use_cache: list | None = None) -> tuple:
        """
        Get wavelengths associated with the pixel column positions
        of args.lines number of the most prominent lines.
        If use_cache is a list with the right number of wavelength
        values, this will use those instead of asking the user.

        Parameters
        ----------
        use_cache : list
            Cached wavelengths
        """
        if self.anchors is None:
            u.halt("Error:\tLine column anchors not yet established.\n\tRun Transform.find_anchors() before this.")

        # args.lines number of lines sorted from most to least prominent
        # lines = sorted(sorted((i for i in self.anchors.items()), key=max, reverse=True)[:args.lines], reverse=True)
        # User-input wavelength values
        vals = []

        def __manual(anchors, vals):
            """Manually enter wavelengths"""

            def __query(anchor):
                """Get user input of wavelength for a line anchor"""
                fig, ax = plt.subplots(tight_layout=True)

                ax.scatter(anchor, self.anchor_row, color="r", edgecolor="k", s=70)

                ax.imshow(self.calib, cmap="gray", aspect="auto")
                plt.title(
                    "Wavelength (Å) of marked line? If blurry, dim, or wide, skip."
                    "\nClose this window when done to enter wavelength.")
                plt.xlabel(self.filename)
                clicker(ax, ["line"], markers=["o"], colors=["red"])
                ax.get_legend().remove()
                plt.show()
                plt.clf()
                plt.close("all")

            u.sout(
                "Enter the wavelengths (Angstroms, Å, 0.1nm) for the highlighted lines."
                "\nUse https://physics.nist.gov/PhysRefData/ASD/lines_form.html"
                "\n or https://www.astrophoto.at/calibration-arne.html for reference."
                f"\nLine column indices: {', '.join(str(a) for a, p in anchors[::-1])}")
            for a, p in anchors:
                __query(a)

                user_in = u.sinput("Wavelength (Å) ([Enter] to skip): ")
                if user_in != "":
                    vals.append((a, float(user_in)))

            if len(vals) < 3:
                u.halt(
                    "Error:\tThree or more lines of known wavelength are required for calibration."
                    "\n\tSpecify more lines with `-l` or consider decreasing the required prominence."
                )

        if use_cache is None:
            __manual(self.anchors, vals)
        elif (use_cache is not None) and (len(use_cache) == args.lines):
            for a, p in self.anchors:
                vals.append((a, np.float32(use_cache.pop(0))))
        else:
            u.sout("Cannot use cached wavelengths -- too few. Defaulting to manual input.")
            __manual(self.anchors, vals)

        x, y = (vals_arr := np.array(vals))[:, 0], vals_arr[:, 1]
        # Check whether wavelength positively correlated to px index
        if np.corrcoef(x, y)[1,0] < 1:
            self.reverse = True
            # x = self.cols-1-x
        
        # y = mx + b
        # m = m0 + c*y  <=  slope changes
        # gives y = (m0*x+b)/(1-c*x)
        def wavelength_fit(xs, *params):
            m0, b0, c1 = params
            return (m0*xs+b0)/(1-c1*xs)
        
        # Param guesses for curve fitting
        lreg = stats.linregress(x, y)
        m0 = lreg.slope
        b0 = lreg.intercept
        c1 = args.resolution_limit

        p0 = (m0, b0, c1)

        with warnings.catch_warnings():
            warnings.filterwarnings("ignore", r"Covariance of the parameters could not be estimated")
            result, _ = curve_fit(wavelength_fit, x, y, p0)

        m0, b0, c1 = result
        
        return y.tolist(), m0, b0, c1, self.anchor_row,     self.anchors, self.reverse
        #     wavelengths, m0, b0, c1,      anchor row, anchor positions, backwards spectrum

    def get_shape(self):
        """Get the shape of each line"""
        # Set max recursion depth to mostly avoid total failure
        sys.setrecursionlimit(5 + self.rows)

        # Get trims if not already present
        if (self.TRIML is None) or (self.TRIMR is None) or (self.TRIMT is None) or (self.TRIMB is None):
            self.TRIML = self.header[v.univ_header_trimCLOW]
            self.TRIMR = self.header[v.univ_header_trimCHI]
            self.TRIMB = self.header[v.univ_header_trimRLOW]
            self.TRIMT = self.header[v.univ_header_trimRHI]
        # Get anchor column indices
        if self.anchors is None or self.anchor_row is None:
            self.anchors = [int(a) for a in self.header[v.univ_header_anchors].split(",")]
        else:
            self.anchors = [a for a, p in self.anchors]
        # Get anchor row
        if self.anchor_row == 0:
            self.anchor_row = self.header[v.univ_header_crrow]
        # Get direction
        if self.direction is None:
            if self.anchor_row == 0:
                self.direction = "down"
            elif self.anchor_row == self.rows-1:
                self.direction = "up"
            else:
                if np.sum(self.calib[self.anchor_row]) > np.sum(self.calib[self.anchor_row-1]):
                    self.direction = "up"
                else:
                    self.direction = "down"

        # Row indices to check for spectrum in the image
        match self.direction:
            case "up":
                indices = np.arange(self.TRIMB, self.rows - self.TRIMT)
            case "down":
                indices = np.flip(np.arange(self.TRIMB, self.rows - self.TRIMT))

        # Set up offsets array
        self.offsets = np.full((self.rows, len(self.anchors)), np.nan)

        N_indices = indices.size
        
        # Size of comparison (must have odd length)
        comp = np.array([-2, -1, 0, 1, 2])
        list_offset = (comp.size - 1) // 2

        # Iterate over anchors
        for i, anchor in enumerate(self.anchors):
            col_offset = 0
            # Get actual position of max value
            # If the values are the same, take the centre
            start = self.calib[self.anchor_row, comp+anchor]
            start_indices = np.argwhere(np.isin(start, np.max(start))).ravel()-list_offset
            start_index = np.take(start_indices, (start_indices.size-1) //2)

            match self.direction:
                case "up":
                    next_row_direction = +1
                case "down":
                    next_row_direction = -1
            
            shifts = []

            def trace_recur(rows=indices, row_idx=0, column=anchor+start_index, anchor=anchor):
                """Internal function.
                Recursively move along each line, finding offset from adjacent row."""
                if abs(row_idx) < N_indices:
                    row = indices[row_idx]
                    # Move along the line by one row and extract values for comparison
                    comp_vals = self.calib[row+next_row_direction, column+comp]
                    if np.abs(np.sum(comp_vals)) > 1e-3:
                        # Check for repeated values along the line's width
                        repeated_values = np.argwhere(np.isin(comp_vals, np.max(comp_vals))).ravel()-list_offset
                        # Take the position in the centre/centre-right
                        col_offset = np.take(repeated_values, (repeated_values.size - 1) // 2)
                        shift = (anchor - column + col_offset)
                        return trace_recur(row_idx=row_idx+1, column=column+col_offset), shifts.append(shift)
                else:
                    pass

            trace_recur()

            match self.direction:
                case "down":
                    self.offsets[indices[:len(shifts)], i] = shifts
                case "up":
                    self.offsets[indices[:len(shifts)], i] = np.flip(shifts, axis=0)
            
        sys.setrecursionlimit(1000)

        return (gradient:=np.nanmedian(np.nan_to_num(self.offsets), axis=1).astype(int)), np.nanmax(gradient).astype(int), np.nanmin(gradient).astype(int)


class MapNode:
            def __init__(self, index, data, pre=None, post=None):
                self.index = index
                self.data = data
                self.pre = pre
                self.post = post

class MapHandler:
    def __init__(self, path: str):
        self._filename: str = path
        self.spectra: list = []
        self.calibs: list = []
        self.cdates: list = []
        self.transforms: list = []
        # self.reverse: list = []
        self.fmap = None
        self.header = ["FILENAME", "PATH", "UT", "JD", "PERIOD", "PHASE",
                       "NEARCDATE", "NEARTRANS", "POSSHIFT", "NEGSHIFT",
                       "NEARCAL", "CALIDX", "M0", "B0", "C1",
                       "TRIML", "TRIMR", "TRIMT", "TRIMB", "REVERSE"]

    def _add(self, path):
        """Add a file path to the filemap"""
        # Get values for row
        ut = u.date(path, form=v.tag_header_UT_date)
        jd = u.date(path, form=v.tag_header_julian_date)
        jd_str: str = f"{jd:#.16g}"
        # Sort by phased period or by Julian date
        phase_str: str = "0"
        period = args.period
        if period - 0. > 0.0001:
            if args.epoch - 0. > 0.0001:
                jd -= args.epoch
            phase = (jd % args.period) / args.period
            # Phase with 16 digits so files can be ordered by name
            phase_str = f"{phase:#.16g}"
            new_path = f"{phase_str}_{v.tag_filename_spectrum}.fits"
        else:
            period, phase = None, None
            # Julian date with 16 digits so files can be ordered by name
            new_path = f"{jd_str}_{v.tag_filename_spectrum}.fits"
        # TODO: CHANGE BACK TO "before"
        near_cal_index = u.nearest(jd, self.cdates, direction="any", give="index")
        near_date = self.cdates[near_cal_index]
        near_trans = u.find(v.location, v.transform_tag, str(near_date), files=self.transforms, mode="in")[0]
        pos_shift, neg_shift = [
            # Get integer shifts from the header of the nearest transformation
            int(c.split("# ")[-1]) for c in pd.read_csv(near_trans, header=0, nrows=0).columns.tolist()
        ]

        # Exception class for handling wrongly-defined header contents
        class ValuesMismatch(Exception):
            pass

        # Get nearest calibration image and associated calibration values
        # NOTE: 'JULDATE' keyword argument must match Values.univ_header_jd.
        #       If you get an error here, check this requirement.
        #       This should be fixed, but I haven't found a nice way yet.
        try:
            near_cal = u.find(
                v.location,
                v.master_calib_tag, files=self.calibs,
                search_header=True, JULDATE=near_date
            )[0]
            nc_head = fits.getheader(near_cal)
            m0, b0, c1 = nc_head[v.univ_header_m0], nc_head[v.univ_header_b0], nc_head[v.univ_header_c1]
            reverse = nc_head["REVERSE"]
            trim_L, trim_R = nc_head[v.univ_header_trimCLOW], nc_head[v.univ_header_trimCHI]
            trim_T, trim_B = nc_head[v.univ_header_trimRHI], nc_head[v.univ_header_trimRLOW]
            # Data row to be written to the filemap
            data = [path, new_path, ut, jd_str, period, phase, near_date, \
                   near_trans, pos_shift, neg_shift, near_cal, near_cal_index, m0, b0, c1, \
                   trim_L, trim_R, trim_T, trim_B, reverse]

            # Set index for file map sorting
            if period is not None:
                index = phase
            else:
                index = jd
            flag, self.fmap = self.__insert(self.fmap, index, data)
        except ValuesMismatch as vm:
            u.sout(f"Error: {vm}", force=True)
        v.n_spectra += 1

    def __insert(self, mnode, index, data):
        """
        Insert a new MapNode in the appropriate place
        Binary search tree => pre goes left, post goes right
        """
        if mnode is None:
            return True, MapNode(index, data)
        else:
            # Phase already in map
            if mnode.index == index:
                # TODO: check which image is better and keep it
                return False, mnode
            # phase less than node phase
            elif index < mnode.index:
                left_flag, left_branch = self.__insert(mnode.pre, index, data)
                if left_flag:
                    mnode.pre = left_branch
                return left_flag, mnode
            # phase greater than node phase
            else:
                right_flag, right_branch = self.__insert(mnode.post, index, data)
                if right_flag:
                    mnode.post = right_branch
                return right_flag, mnode

    def populate(self):
        """Populate file map"""
        for sp in self.spectra:
            self._add(sp)

    def _inorder(self, mnode):
        """In-order traversal of map tree"""
        out = []
        if mnode is not None:
            out = self._inorder(mnode.pre)
            out.append(np.array(mnode.data, dtype=object))
            out = out + self._inorder(mnode.post)
        return out

    def writeout(self, mnode):
        df = pd.DataFrame(np.vstack(self._inorder(mnode)), columns=self.header)
        path = os.path.join(v.location, self._filename)
        df.to_csv(path, index_label="INDEX")
        v.storage_used += os.path.getsize(path)


class Spectrum:
    def __init__(self, spectrum, transform, filemap):
        self.sp_path = spectrum
        self.t_path = transform
        self.map = filemap
        self.index = 0
        self.header = fits.getheader(self.sp_path, memmap=False)
        self.spectrum = fits.getdata(self.sp_path, memmap=False)
        self.transformation = np.array(pd.read_csv(transform)).astype(int)
        self.trimmed = False
        self.cutoff_pos = None
        self.cutoff_neg = None
        self.TRIMR = 0
        self.TRIML = 0
        self.TRIMT = 0
        self.TRIMB = 0
        self.m0 = self.map.at[self.index, v.univ_header_m0]
        self.b0 = self.map.at[self.index, v.univ_header_b0]
        self.c1 = self.map.at[self.index, v.univ_header_c1]
        self.reverse = False

    def transform(self, orient):
        """Apply transform to spectrum"""
        rows, cols = self.spectrum.shape
        lambda_indices_full = np.arange(0, cols)
        if self.reverse:
            self.TRIML, self.TRIMR = self.TRIMR, self.TRIML
            # lambda_full = np.flip(lambda_full, axis=0)
            # self.spectrum = np.flip(self.spectrum, axis=1)

        # Transformed NumPy array representing FITS spectrum
        u.move(self.sp_path, os.path.join(v.location, v.backup_path))
        if orient == "v":
            self.spectrum = self.spectrum.T
        transformed = np.vstack([self.__shift(self.spectrum[r], self.transformation[r][0]) for r in range(rows)])
        del self.transformation
        lambda_indices_trimmed, trimmed = self.__trim(lambda_indices_full, transformed)
        del transformed

        # Consider only rows containing signal
        cropped = self.__extractRows(trimmed)
        
        # if self.reverse:
        #     self.TRIML, self.TRIMR = self.TRIMR, self.TRIML
            # cropped = np.flip(cropped, axis=0)

        if args.artificial_bias:
            cropped -= args.artificial_bias
        cropped[cropped<0] = 0

        # Collapse to 1D spectrum
        collapsed_path = os.path.join(v.location, self.map.at[self.index, "PATH"])
        collapsed = self.__collapse(cropped)
        
        # Update header values
        self.populate_header()

        # Update equation after trimming indices; build header for image
        y1 = u.wavelength_calib_equation(x1:=self.TRIML, self.m0, self.b0, self.c1)
        y2 = u.wavelength_calib_equation(x2:=(cols-self.TRIML-self.TRIMR)//2, self.m0, self.b0, self.c1)
        y3 = u.wavelength_calib_equation(x3:=cols-self.TRIMR, self.m0, self.b0, self.c1)

        # y = mx + b
        # m = m0 + c*y  <=  slope changes
        # gives y = (m0*x+b)/(1-c*x)
        def wavelength_fit(xs, *params):
            m0, b0, c1 = params
            return (m0*xs+b0)/(1-c1*xs)
        
        # Param guesses for curve fitting
        p0 = (self.m0, self.b0, self.c1)
        print(p0)

        with warnings.catch_warnings():
            warnings.filterwarnings("ignore", r"Covariance of the parameters could not be estimated")
            result, _ = curve_fit(wavelength_fit, (x1-self.TRIML-1, x2-self.TRIML-1, x3-self.TRIML-1), (y1, y2, y3), p0)

        m0n, b0n, c1n = result
        print(result)

        lambda_trimmed = u.wavelength_calib_equation(np.arange(lambda_indices_trimmed.size), m0n, b0n, c1n)
        plt.scatter((x1-self.TRIML,x2-self.TRIML,x3-self.TRIML), (y1,y2,y3))
        plt.plot(lambda_trimmed)
        plt.show()

        plt.plot(lambda_trimmed, collapsed)
        plt.show()
        quit()
        # Image header
        img_header = self.header.copy()
        # Have to add the difference between old and new (on trimmed indices) b0
        # self.b0 -= (u.wavelength_calib_equation(self.TRIML, self.m0, self.b0, self.c1) - self.b0)
        img_header["B0"] = self.b0
        img_header["C1"] = self.c1
        img_header["M0"] = self.m0

        # Write FITS file with all the data
        hdul = fits.HDUList()
        hdu1 = fits.ImageHDU(data=cropped, header=img_header, name="spectrum2d")
        hdu2col1 = fits.Column(name="lambda", format="D", unit="Angstrom", array=lambda_trimmed.astype(np.float64))
        hdu2col2 = fits.Column(name="counts", format="J", unit="count", array=collapsed.astype(np.int32))
        hdu2 = fits.TableHDU.from_columns([hdu2col1, hdu2col2], header=self.header)
        hdul.append(hdu1)
        hdul.append(hdu2)
        hdul.writeto(collapsed_path, overwrite=v.overwrite)

        del collapsed
        v.storage_used += os.path.getsize(collapsed_path)
        del collapsed_path

    def __shift(self, specrow, shiftrow, fill=0):
        """Internal function to shift rows"""
        out = np.empty_like(specrow)
        if shiftrow > 0:
            out[:shiftrow] = fill
            out[shiftrow:] = specrow[:-shiftrow]
        elif shiftrow < 0:
            out[shiftrow:] = fill
            out[:shiftrow] = specrow[-shiftrow:]
        else:
            out[:] = specrow
        return out

    def __trim(self, lambda_full, transformed):
        """Internal function to trim the spectrum based on calibration image size"""
        # Ensure the trimming is the largest of (trims, shifting)
        if self.cutoff_pos > self.TRIML:
            self.TRIML = self.cutoff_pos
        if np.abs(self.cutoff_neg) > self.TRIMR:
            self.TRIMR = np.abs(self.cutoff_neg)

        rows, cols = self.spectrum.shape

        spec_trimmed = transformed[self.TRIMB:rows-self.TRIMT, self.TRIML:cols-self.TRIMR]
        lambda_trimmed = lambda_full[self.TRIML:cols-self.TRIMR]

        return lambda_trimmed, spec_trimmed

    def __extractRows(self, trimmed):
        def apply_mask(arr, mask):
            return np.ma.masked_array(arr, mask=~mask)

        def largest_contiguous_area_mask(blurred):
            # Find all True regions in the array
            labeled_array, num_features = ndimage.label(blurred)
            if num_features == 0:
                return np.zeros_like(blurred)
            # Calculate the size of each region
            region_sizes = np.bincount(labeled_array.ravel())[1:]
            # Find the label of the largest region
            largest_region_label = np.argmax(region_sizes) + 1
            # Create a mask for the largest region
            largest_region_mask = labeled_array == largest_region_label
            return largest_region_mask

        def fill_holes(arr):
            return ndimage.binary_fill_holes(arr.astype(bool))

        def get_col_slope(contiguous):
            """
            Get the intersections of lines fit to the sloped edges
            of columnar sums for use in spectrum angle estimate.
            
            Returns (incr zero, incr top, decr top, decr zero) intersections
                    ◱          ◰        ◳         ◲   in  __//^^\\__
            """
            def fit_edge_linear(col_sum, edge):
                rvalue = 1.0
                counter = 1
                while (rvalue - 0.995 > 1e-4) and (counter <= 10000):
                    px_near_incr = np.arange(start=edge-counter, stop=edge+1+counter)
                    vals_near_edge = col_sum[px_near_incr]

                    reg_near_edge = stats.linregress(px_near_incr, vals_near_edge)
                    rvalue = reg_near_edge.rvalue
                    counter += 1
                return reg_near_edge

            def get_intersect(a1, a2, b1, b2):
                """
                Credit: https://stackoverflow.com/a/42727584
                Returns the point of intersection of the lines passing through a2,a1 and b2,b1.
                a1: [x, y] a point on the first line
                a2: [x, y] another point on the first line
                b1: [x, y] a point on the second line
                b2: [x, y] another point on the second line
                """
                s = np.vstack([a1,a2,b1,b2])        # s for stacked
                h = np.hstack((s, np.ones((4, 1)))) # h for homogeneous
                l1 = np.cross(h[0], h[1])           # get first line
                l2 = np.cross(h[2], h[3])           # get second line
                x, y, z = np.cross(l1, l2)          # point of intersection
                if z == 0:                          # lines are parallel
                    return (float('inf'), float('inf'))
                return x/z, y/z
            
            # Get positions of bounds for linear fit
            # col_sum = signal.savgol_filter(np.sum(contiguous, axis=1), contiguous.shape[0]//20, 2)
            col_sum = np.sum(contiguous, axis=1)
            grad = np.gradient(col_sum)
            incr_edge = np.argmax(grad) # increasing edge
            decr_edge = np.argmin(grad) # decreasing edge
            flat_top = np.average((incr_edge, decr_edge)).astype(int)
            mask_pos = np.argwhere(contiguous)[:,1]
            start = mask_pos[0]
            stop = mask_pos[-1]
            if start > stop:
                start = stop
                stop = mask_pos[0]
            px_masked = np.arange(start=start, stop=stop+1)

            # Linear regressions to sloped edges and flat(ish) top
            # Shaped like a squished tophat
            # Regression centred at inflection point in gradient of edges (min/max gradient)
            # -> Line expands outward 1 px in each direction until R^2<0.995
            # Edges change sinusoidally. See theory
            lreg_incr = fit_edge_linear(col_sum, incr_edge)
            lreg_decr = fit_edge_linear(col_sum, decr_edge)
            lreg_flat = fit_edge_linear(col_sum, flat_top)

            # Lines
            line_incr = lreg_incr.slope*px_masked+lreg_incr.intercept
            line_decr = lreg_decr.slope*px_masked+lreg_decr.intercept
            line_flat = lreg_flat.slope*px_masked+lreg_flat.intercept

            # Intersections
            intersect_incr_zero = (-lreg_incr.intercept/lreg_incr.slope, 0)
            intersect_decr_zero = (-lreg_decr.intercept/lreg_decr.slope, 0)
            intersect_incr_flat = get_intersect((px_masked[0], line_incr[0]), (px_masked[-1], line_incr[-1]), (px_masked[0], line_flat[0]), (px_masked[-1], line_flat[-1]))
            intersect_decr_flat = get_intersect((px_masked[0], line_decr[0]), (px_masked[-1], line_decr[-1]), (px_masked[0], line_flat[0]), (px_masked[-1], line_flat[-1]))

            return intersect_incr_zero, intersect_incr_flat, intersect_decr_flat, intersect_decr_zero

        # Major functions
        # Angle and rectangle mask
        def spectrum_angle(arr, n_slices=100):
            """Get angle of the spectrum by slicing the input
            arr into n-slices pieces and finding COM in each"""
            arr[arr<np.average(arr)] = 0
            slices = np.array_split(arr, n_slices, axis=1)
            coms_rough = np.array([
                (ndimage.center_of_mass(slc), slc.shape) for slc in slices
            ], dtype=tuple)
            # [ [ com_y, com_x ], [ slc_h, slc_w ] ]
            coms = np.array([
                (com[0][1]+(i*com[1][1]), com[0][0]) for i, com in enumerate(coms_rough)
            ])
            com_reg = stats.linregress(coms)
            angle = np.arctan(com_reg.slope) # radians
            up_or_down = np.sign(com_reg.slope) # 1.0/-1.0 <-> up/down from left-to-right

            return angle, up_or_down

        # Get angled rectanglular mask
        def get_rect_mask(trimmed, angle, x1, x2, up_or_down: int=1, margin_pct: float=5, aa_scale: int=2):
            """Get the aliased multiplicative [0,1] rectangular spectrum mask"""
            def get_rect(x, y, width, height, angle):
                """Generate an angled rectangle"""
                # https://stackoverflow.com/a/12655480
                rect = np.array([(0, 0), (width, 0), (width, height), (0, height), (0, 0)])
                R = np.array([[np.cos(angle), -np.sin(angle)],
                            [np.sin(angle), np.cos(angle)]])
                offset = np.array([x, y])
                transformed_rect = np.dot(rect, R) + offset
                return transformed_rect

            r, c = trimmed.shape
                
            # Mask offsets to ensure full coverage
            # -> see theory
            rect_x_offset = (a:=(h:=x2-x1)*np.tan(angle))*(np.cos(angle)) + h
            rect_y_offset = (a*np.sin(angle)) + h*np.tan(angle)

            # Rectangular mask
            # Changing size by aascale for later downscaling for antialiasing
            img = Image.fromarray(np.zeros((aa_scale*r, aa_scale*c)))
            # Set margin
            margin = 0
            if margin_pct:
                margin = c//(100/margin_pct) # margin_pct% of image height margin on each side of rect filter
            rect = get_rect(x=-rect_x_offset, y=aa_scale*(x1-(margin/2)-rect_y_offset), width=aa_scale*(c+rect_x_offset), height=aa_scale*(x2-x1+2*margin), angle=up_or_down*angle)
            
            drawing = ImageDraw.Draw(img)
            drawing.polygon([tuple(p) for p in rect], fill=1)
            # Resizing for Lanczos antialiasing
            antialiased = np.abs(np.asarray(img.resize(trimmed.shape[::-1], resample=Image.LANCZOS)))
            
            return antialiased
        
        def fill_contours(arr):
            """https://stackoverflow.com/a/61471901"""
            return np.maximum.accumulate(arr, 1) &\
                np.maximum.accumulate(arr[:, ::-1], 1)[:, ::-1] &\
                np.maximum.accumulate(arr[::-1, :], 0)[::-1, :] &\
                np.maximum.accumulate(arr, 0)

        # Median blur if requested
        if args.blur:
            spec_trim = ndimage.median_filter(trimmed, size=args.blur)
        else:
            spec_trim = trimmed

        # For testing on calibration spectra
        return spec_trim

        blurred = spec_trim-np.percentile(spec_trim, 96)
        blurred[blurred<0] = 0

        # Get contiguous spectrum mask without holes
        holey_mask = largest_contiguous_area_mask(blurred)
        unholey_mask = fill_holes(holey_mask)
        filled_mask = fill_contours(unholey_mask)

        masked = spec_trim*filled_mask
        
        if not args.ultra_low_snr:
            self.spectrum = masked[np.sum(masked, axis=1)>args.rows_sum_inclusion_threshold, :]
            return self.spectrum
        else:
            # Apply the mask
            data = apply_mask(spec_trim, filled_mask)
            data = data.filled()

            intersect_incr_zero, intersect_incr_flat, intersect_decr_flat, intersect_decr_zero = get_col_slope(data)

            x0, y0 = intersect_incr_zero
            x1, y1 = intersect_incr_flat
            x2, y2 = intersect_decr_flat
            x3, y3 = intersect_decr_zero

            # Which angle fits? Up or down?
            ## Look at contiguous spec direction
            ## up_or_down tells which angle to take: negative or positive
            angle, up_or_down = spectrum_angle(data)

            # Build multiplicative (slow but nice) antialiased diagonal rectangle mask
            rect_mask = get_rect_mask(spec_trim, angle, x1, x2, up_or_down=up_or_down, margin_pct=1)

            # Rectangular mask over data
            # Not a boolean mask -- cannot use apply_mask()
            data_rect = data*rect_mask
            cropped = data[np.sum(rect_mask, axis=1)!=0]
            self.spectrum = cropped

            return cropped #np.where(~cropped.mask, cropped, 0)

    def __collapse(self, cropped):
        """Internal function to collapse a spectrum to its 1D form"""
        # no_zeroes = (summed := np.sum(cropped, axis=0))[summed != 0]
        # first_nonzero = summed.view(bool).argmax()//summed.itemsize
        # first_nonzero = first_nonzero if summed[first_nonzero] else 0
        # self.b0 += self.c1*(first_nonzero-self.m0)
        # return no_zeroes

        return np.median(cropped, axis=0)

    def __dont_collapse(self, cropped):
        """DON'T USE THIS (for transform)"""
        return cropped

    def populate_header(self):
        self.header["BITPIX"] = 16
        # Ammend FITS header to make note of processing
        self.header.append(
            ("HISTORY", 1, "Row shifted to correct optical distortion.")
        )
        self.header.append(
            ("ORIGNAME", os.path.basename(self.sp_path), f"{'NOT ' if not v.backup_spectra else ''}Moved")
        )

        if self.reverse:
            self.header["REVERSE"] = True

        # Pixel-to-wavelength conversion
        # Don't add these to the header. Resolve this from the wavelengths in the TableHDU instead.
        # self.header.append((v.univ_header_m0, self.m0, "Reference pixel location"))
        # self.header.append((v.univ_header_b0, self.b0, "Reference pixel value (Angstrom)"))
        # self.header.append((v.univ_header_c1, self.c1, "Width of a pixel (Angstrom/px)"))


##############################################################################
# Helper functions

def stack_calibs(date: str, interval: int, calibs: list, mode: str, cmap: CalibMap) -> None:
    """
    Median stack all calibs given as calibs.

    Parameters
    ----------
    date : str
        Date of calibration images (for filenames)
    interval : int
        Number of calibration images to stack. e.g.:
        - 1 for every image being distinct
        - 3 to indicate 3 calibration spectra taken together to be stacked
    calibs : list
        Calibration image filepath(s)
    mode : str
        'h' for horizontal, 'v' for vertical.
        Spectra will be forced horizontal.
    cmap : CalibMap
        Calibration map object
    """
    all_calibs = np.array([u.read(c, "FITS") for c in calibs], dtype=list)
    headers, imgs = all_calibs[:, 0].tolist(), all_calibs[:, 1].tolist()
    # Group headers
    header_groups = headers[interval - 1::interval]
    if len(header_groups) * interval < len(headers):
        header_groups.append(headers[-1])
    # Group images
    img_groups = [imgs[i:i + interval] for i in range(0, len(imgs), interval)]
    # Iterate over each group of calibration images for given 'date'
    for index in range(len(header_groups)):
        # Header of LAST image in series
        new_header = header_groups[index]
        c_imgs = img_groups[index]
        len_c_imgs = len(c_imgs)
        # Stack all images in the series
        stacked = u.median_stack(c_imgs)
        new_filename = os.path.join(v.location, f"{v.master_calib_tag}_{date}_{index:04d}.fits")
        # Write a row to the calibration map
        ut = date
        jd = float(f"{new_header[v.tag_header_julian_date]:#.16g}")
        tname = f"{v.transform_tag}_{jd}.csv"
        tpath = os.path.join(v.location, tname)
        row = [new_filename, ut, jd, tpath]
        cmap.write(row)
        # Note genesis of the master calibration file
        new_header.append(("HISTORY", "Median stacked if more than one component file"))
        # Store universal UT and JD in FIT(S) header
        new_header.append((v.univ_header_ut, ut, "Universal time date"))
        new_header.append((v.univ_header_jd, jd, "Julian date"))
        # Note components of calibration images in the master FITS header
        for i in range(len_c_imgs):
            new_header.append((f"COMPNT{i + 1}", len_c_imgs, Path(calibs[index + i]).stem), end=True)
        # Account for the mode, always writing HORIZONTAL spectra
        if mode == "v":
            h_ax = new_header["NAXIS1"]
            v_ax = new_header["NAXIS2"]
            new_header["NAXIS1"] = v_ax
            new_header["NAXIS2"] = h_ax
            fits.writeto(new_filename, stacked.T, header=new_header, overwrite=True)
        elif mode == "h":
            fits.writeto(new_filename, stacked, header=new_header, overwrite=True)
        # Record size of new file
        v.storage_used += os.path.getsize(new_filename)
        v.storage_used += os.path.getsize(cmap.path)
    del all_calibs, headers, imgs


def check_mode(header: Any, mode: str) -> str:
    """
    Check the mode given in command line arguments.
    If the mode does not match with file shape, suggest a change.

    Parameters
    ----------
    header : FITS header
        Header of a FITS file to check
    mode : str
        Mode set by user with command line arguments (or default = 'h')
    """
    if mode == "h" and header["NAXIS1"] < header["NAXIS2"]:
        u.sout("It seems like your images are vertical.")
        user_in = u.sinput("Change mode from 'h' to 'v'? (Y/N): ")
        if user_in.strip().upper() == "Y":
            mode = "v"
        else:
            u.sout("Calibration may not work properly.")
    elif mode == "v" and header["NAXIS2"] < header["NAXIS1"]:
        u.sout("It seems like your images are horizontal.")
        user_in = u.sinput("Change mode from 'v' to 'h'? (Y/N): ")
        if user_in.strip().upper() == "Y":
            mode = "h"
        else:
            u.sout("Calibration may not work properly. Continuing...")
    return mode


def trim_calib(path: str, use_cache: dict | None = None, overwrite: bool = True) -> dict:
    """
    Trim a calibration image given its path.

    Parameters
    ----------
    path : str
        Path to calibration image to trim
    use_cache : dict | None
        Cached trims from past calibration trimming or None
        > e.g. {trim_L, trim_R, trim_T, trim_B}
        Default: None
    overwrite : bool
        Whether to tell trimming to overwrite original
        Default: True

    Returns
    -------
    out : dict
        Returns {calib: np.ndarray, trims: {trim_L:int, trim_R:int, trim_T:int, trim_B:int}}
    """
    c = Calib(path)
    if use_cache is None:
        corners = c.get_corners_manual()
        c.set_trims(corners)
    else:
        trims = use_cache.values()
        c.TRIML, c.TRIMR, c.TRIMT, c.TRIMB = trims
    return c.trim(overwrite=overwrite)


def wavelength_calib_manual(path: str, use_cache: list | None = None) -> list:
    """
    Get the wavelength calibration of each calibration image.

    Parameters
    ----------
    path : str
        Path to calibration image
    use_cache: list | None
        Cached wavelenghts from a previous run or None

    Returns
    -------
    use_cache : list
        Cached list of wavelengths for next calibration image
    """
    c = Calib(path)
    c.find_anchors()
    use_cache, m0, b0, c1, row, anchors, reverse = c.get_wavelengths(use_cache=use_cache)
    # Amend FIT(S) header
    c.header.append((v.univ_header_b0, b0, "Wavelength (Angstrom) at edge of image"))
    c.header.append((v.univ_header_m0, m0, "Pixel scale change at edge of image"))
    c.header.append((v.univ_header_c1, c1, "Rate of pixel scale change"))
    c.header.append((v.univ_header_crrow, row, "Image row of reference pixel"))
    c.header.append((v.univ_header_anchors, ','.join(str(a) for a, p in anchors), "Column indices of anchors"))
    c.header.append((v.univ_header_reverse, reverse, "Corr(lam, px)<0"))
    if os.path.exists(path):
        size_before = os.path.getsize(path)
    else:
        size_before = 0
    path_link = Path(path)
    path_link.unlink(missing_ok=True)
    fits.writeto(path, c.calib, header=c.header, overwrite=True)
    v.storage_used += os.path.getsize(path) - size_before
    return use_cache


# ##############################################################################
# Main functions

def own_calib(orient):
    """Find and stack calibration images for mode 'OWN'"""
    u.sout_header("Calibration", fill="-")
    # Initialise a new calibration map
    cmap = CalibMap(os.path.join(v.location, v.calib_map_name))
    cmap.clear()
    # Calibration map header
    cmap.write([v.univ_header_path, v.univ_header_ut, v.univ_header_jd, v.univ_header_transform])

    # Get calibration images either by searching or from provided paths
    if not args.calibs:
        cfiles = u.find(v.location, ".fit", v.tag_filename_calib)
        # Omit master calibs
        cfiles = [c for c in cfiles if v.master_calib_tag not in c]
    else:
        cfiles = [*args.calibs]
    if len(cfiles) == 0:
        u.halt(f"No calibration images found in '{v.location}'.")
    # Check orientation of first calibration image
    # If it does not align with mode, suggest a change.
    header = fits.getheader(cfiles[0])
    orient = check_mode(header, orient)

    # Get unique calibration dates
    cdates = u.get_dates(cfiles)
    u.sout(f"Found {len(cfiles)} new calibration image(s).")

    # Get name-date-index and write master calibration images for DAO data
    # Images indexed by date and then by number (YYYY-MM-DD_XXXX)
    for cd in cdates:
        # Get all calibration images for a given date
        calibs = u.find(
            v.location, ".fit", v.tag_filename_calib,
            files=cfiles, search_header=True, mode="in", DATEàààOBS=cd
        )
        stack_calibs(cd, v.combined_calibs, calibs, orient, cmap)
        del calibs
    del cdates, cfiles
    u.sout("Generated master calibration images and calibration map.")


def dao_calib(orient):
    """Find and stack calibration images for mode 'DAO'"""
    u.sout_header("Calibration", fill="-")
    # Initialise a new calibration map
    cmap = CalibMap(os.path.join(v.location, v.calib_map_name))
    cmap.clear()
    # Calibration map header
    cmap.write([v.univ_header_path, v.univ_header_ut, v.univ_header_jd, v.univ_header_transform])

    # Get calibration images either by searching or from provided paths
    if not args.calibs:
        cfiles = u.find(v.location, ".fit", search_header=True, OBSTYPE=v.tag_header_calib)
        # omit master calibs
        cfiles = [c for c in cfiles if v.master_calib_tag not in c]
    else:
        cfiles = [*args.calibs]
    if len(cfiles) == 0:
        u.halt(f"No calibration images found in '{v.location}'.")
    # Check orientation of first calibration image
    # If it does not align with mode, suggest a change.
    header = fits.getheader(cfiles[0])
    orient = check_mode(header, orient)

    # Get unique calibration dates
    cdates = u.get_dates(cfiles)
    u.sout(f"Found {len(cfiles)} new calibration image(s).")

    # Get name-date-index and write master calibration images for DAO data
    # Images indexed by date and then by number (YYYY-MM-DD_XXXX)
    for cd in cdates:
        # Get all calibration images for a given date
        calibs = u.find(
            v.location, ".fit",
            files=cfiles, search_header=True, mode="in",
            OBSTYPE=v.tag_header_calib, DATEàààOBS=cd
        )
        stack_calibs(cd, v.combined_calibs, calibs, orient, cmap)
        del calibs
    del cdates, cfiles
    u.sout("Generated master calibration images and calibration map.")


def get_transforms_manual(mcalibs_exist: bool = False):
    """
    Get transformations from master calibration images
    mcalibs_exist: bool: Whether master calibration images already exist
    """
    cmap_path = u.find(v.location, v.calib_map_name)
    if len(cmap_path) >= 1:
        cmap_path = cmap_path[0]
    else:
        u.halt("No calibration image map exists. Generate one before making transformations.")
    cmap = pd.read_csv(cmap_path, header=0)

    def __trim(cmap):
        """Internal function to overwrite existing
        calibration images with trimmed ones"""
        cpaths = np.array(cmap.loc[:, v.univ_header_path])
        use_cache = None
        for cp in cpaths:
            if args.cache:
                if use_cache is not None:
                    user_in = u.sinput(f"'{Path(cp).stem}': Use cached trim boundaries from last calibration image? (Y/N): ")
                    if user_in.upper() != "Y":
                        use_cache = None
            else:
                use_cache = None
            _, use_cache = trim_calib(cp, use_cache=use_cache, overwrite=True).values()

    def __wavelen(cmap):
        """Internal function to add appropriate wavelength
        calibrations to master calibration image headers"""
        cpaths = np.array(cmap.loc[:, v.univ_header_path])
        use_cache = None
        for cp in cpaths:
            if args.cache:
                if use_cache is not None:
                    u.splot(fits.getdata(cp), mode="sum")
                    user_in = u.sinput(f"'{Path(cp).stem}': Use cached wavelength calibration from last image? (Y/N): ")
                    if user_in.upper() != "Y":
                        use_cache = None
            else:
                use_cache = None
            use_cache = wavelength_calib_manual(cp, use_cache=use_cache)

    def __shape(cmap):
        """Internal function to find shape of calibs"""
        cpaths = np.array(cmap.loc[:, v.univ_header_path])
        n = len(cpaths)
        u.sout(f"Generating {n} transformation{'s' if n>1 else ''}.")
        tnames = np.array(cmap.loc[:, v.univ_header_transform])
        for i in range(len(cmap)):
            cp = cpaths[i]
            c = Calib(cp)
            transform, max_pos_shift, max_neg_shift = c.get_shape()
            tn = tnames[i]
            tpath = os.path.join(v.location, tn)
            np.savetxt(tpath, transform, header=f"{max_pos_shift},{max_neg_shift}")
            v.storage_used += os.path.getsize(tpath)

    if not mcalibs_exist:
        u.sout_header("Trimming", fill="-")
        __trim(cmap)
        u.sout_header("Wavelength", fill="-")
        __wavelen(cmap)
    u.sout_header("Image Shapes", fill="-")
    __shape(cmap)


def build_filemap():
    """Build a filemap with transformations and object spectra"""
    u.sout_header("Building Filemap", fill="-")
    # Initialise a MapHandler object
    global mh
    mh = MapHandler(os.path.join(v.location, "filemap.csv"))
    # Get object filenames
    if args.spectra is not None:
        mh.spectra = args.spectra
    else:
        if args.dao:
            mh.spectra = u.find(v.location, ".fit", search_header=True, OBSTYPE="object")
        else:
            mh.spectra = u.find(v.location, ".fit", v.tag_filename_spectrum)
    assert len(mh.spectra), "No object spectra found"
    u.sout(f"Found {len(mh.spectra)} object spectra.")
    # Get filenames and dates for filemap
    cmap_path = u.find(v.location, v.calib_map_name)
    if len(cmap_path) >= 1:
        cmap_path = cmap_path[0]
    else:
        u.halt("No calibration image map exists. Generate one before making a file map.")
    cmap = pd.read_csv(cmap_path, header=0)
    mh.calibs = np.array(cmap.loc[:, v.univ_header_path]).tolist()
    mh.cdates = np.array(cmap.loc[:, v.univ_header_jd]).tolist()
    mh.transforms = np.array(cmap.loc[:, v.univ_header_transform]).tolist()
    # mh.reverse = [fits.getheader(cpath)["REVERSE"] for cpath in mh.calibs]

    # Set period if defined
    try:
        args.period = float(args.period)
    finally:
        pass

    # Add entries to the filemap
    if args.period - 0 > 0.0001:
        u.sout(f"Set the system period to {args.period} days. Sorting filemap by phased period.")
    else:
        u.sout("No system period specified. Sorting by Julian date.")
    mh.populate()
    mh.writeout(mh.fmap)


def apply_transforms(orient: str):
    """Apply transforms to objects according to filemap."""
    # Load file map into memory
    fmpath = os.path.join(v.location, v.filemap_name)
    fmap = pd.read_csv(fmpath, header=0, index_col="INDEX")

    # Check for folder v.backup_path
    if not os.path.exists(v.backup_path):
        os.mkdir(v.backup_path)

    already_transformed = len(os.listdir(v.backup_path)) != 0
    if not already_transformed:
        warnings.simplefilter(action = "ignore", category = RuntimeWarning)
        t1 = time.default_timer()
        count = 0
        for i in range(v.n_spectra):
            t2 = time.default_timer()
            spectrum = fmap.loc[i, "FILENAME"]
            L.log(f"opened {spectrum}")
            transform = fmap.loc[i, "NEARTRANS"]
            s = Spectrum(spectrum, transform, fmap)
            s.index = i
            del spectrum
            del transform
            s.cutoff_pos = fmap.loc[i, "POSSHIFT"]
            s.cutoff_neg = fmap.loc[i, "NEGSHIFT"]
            s.TRIMR = fmap.loc[i, "TRIML"]
            s.TRIML = fmap.loc[i, "TRIMR"]
            s.TRIMT = fmap.loc[i, "TRIMT"]
            s.TRIMB = fmap.loc[i, "TRIMB"]
            s.reverse = fmap.loc[i, "REVERSE"]
            s.transform(orient)
            del s
            count += 1
            dt = (time.default_timer() - t2) * 10 ** 3
            u.sout(f"{count}/{v.n_spectra} spectra transformed. ({dt:.2f} ms)", end="\r")
        dt = (time.default_timer() - t1) * 10 ** 3
        u.sout(f"{count}/{v.n_spectra} spectra transformed. ({dt:.2f} ms, {10 ** 3 * v.n_spectra / dt:.2f}/s)")
    else:
        u.sout(
            f"Spectra are already transformed."
            f"\nIf this is an error, check spectra to see if they"
            f"\nhave been transformed. Then delete '{Path(v.backup_path).stem}' ONLY IF it is empty."
            f"\nIf it is not empty, move and overwrite its contents into the main directory and try again.")


##############################################################################
# Run a script

def main():
    try:
        # Track time and memory usage
        t0 = time.default_timer()
        tracemalloc.start()

        # Command line arguments
        parser = argparse.ArgumentParser()
        parser.add_argument(
            "--silent", dest="silent", action="store_true",
            help="'--silent' for minimal command line output."
        )
        parser.add_argument(
            "--location", dest="location", default=None, type=str,
            help="Folder directory containing spectra and calibration images"
        )
        parser.add_argument(
            "--vertical", dest="vertical", action="store_true",
            help="Flag to tell the program to expect vertical spectra."
        )
        parser.add_argument(
            "--calibs", dest="calibs", default=None, nargs="+",
            help="A list of calibration images to use"
        )
        parser.add_argument(
            "--spectra", dest="spectra", default=None, nargs="+",
            help="A list of spectra to use"
        )
        parser.add_argument(
            "-b", "--blur", dest="blur", default=3, type=int,
            help="kernel size of median blurring to apply to images. Default: 3."
                 "Set to 0 to disable."
        )
        parser.add_argument(
            "--hotpixfix", dest="hotpixfix", action="store_true",
            help="Not Implemented. Enable hot/cold pixel removal when stdev of its row and col are >1.25*sigma."
        )
        parser.add_argument(
            "--sigma_clip", dest="sigma_clip", default=1.25, type=float,
            help="Not Implemented. Sigma clipping size (# std dev) for inclusion of a row in a spectrum."
        )
        parser.add_argument(
            "-l", "--lines", dest="lines", default=3, type=int,
            help="How many calibration lines to use in wavelength calibration"
        )
        parser.add_argument(
            "--period", dest="period", default=0., type=float,
            help="System period. Default: 0. Set to 0 to disable."
        )
        parser.add_argument(
            "--epoch", dest="epoch", default=0, type=float,
            help="Epoch of period"
        )
        parser.add_argument(
            "--frame", dest="frame", action="store_true",
            help="No longer optional."
        )
        parser.add_argument(
            "--prominence", dest="prominence", default=200, type=int,
            help="Expected prominence of lines in a calibration image. This is the difference in value between a line and its nearest neighbour pixels."
        )
        parser.add_argument(
            "--offset_smoothing", dest="offset_smoothing", default=3, type=int,
            help="Number of points to include when smoothening pixel offsets to remove noise. Recommended >7"
        )
        parser.add_argument(
            "--cache", dest="cache", action="store_true",
            help="'--cache' to offer to use cached values for user repeated actions. Has no effect if '--auto' (not implemented) is used."
        )
        parser.add_argument(
            "--dao", dest="dao", action="store_true",
            help="'--dao' to tell the program to expect DAO spectra."
        )
        parser.add_argument(
            "--auto", dest="auto", action="store_true",
            help="'--auto' to enable fully automatic processing of spectra. Not currently implemented"
        )
        parser.add_argument(
            "--combined_calibs", dest="combinedcalibs", default=3, type=int,
            help="Size of sets of calibration images to expect and stack."
        )
        parser.add_argument(
            "--ultra_low_snr", dest="ultra_low_snr", default=False, action="store_true",
            help="--ultra_low_snr to attempt recover of spectra from images with extremely low (~1.05) SNR"
        )
        parser.add_argument(
            "--artificial_bias", dest="artificial_bias", default=0, type=int,
            help="Artificially apply a bias value to the image."
        )
        parser.add_argument(
            "--rows_sum_inclusion_threshold", dest="rows_sum_inclusion_threshold", default=0, type=int,
            help="Minimal sum of row values to be included in the image -- to cut out perpendicular ghost images"
        )
        parser.add_argument(
            "--resolution_limit", dest="resolution_limit", default=(1/18000), type=float,
            help="Spectral resolution limit of the spetrograph. Defaults to 1/18000."
        )

        # Set up global objects
        global args, u, v, L
        args = parser.parse_args()
        u = Utilities()
        if args.dao:
            v = Values("DAO")
        else:
            v = Values("OWN")
        if args.location is not None:
            v.location = args.location
        L = Log()

        if args.silent:
            L.log("Silent mode")
            u.sout("Spec Process ... ", end="", force=True)
        else:
            u.sout_header("Spec Process")
            u.sout(f"Date: {datetime.now()}")

        if args.auto:
            u.sout("Automatic processing is not yet implemented.\nDefaulting to semi-manual processing.")

        if args.vertical:
            orient = "v"
        else:
            orient = "h"

        v.combined_calibs = args.combinedcalibs

        def filemap_exists():
            fmpath = os.path.join(v.location, v.filemap_name)
            if os.path.isfile(fmpath):
                v.n_spectra = len(pd.read_csv(fmpath).index)
                return True
            else:
                return False

        def transforms_exist():
            cmpath = os.path.join(v.location, v.calib_map_name)
            if os.path.isfile(cmpath):
                names = pd.read_csv(cmpath).loc[:, v.univ_header_transform]
                if np.array([os.path.isfile(n) for n in names]).all():
                    return True
            return False

        def mcalibs_exist():
            cmpath = os.path.join(v.location, v.calib_map_name)
            if os.path.isfile(cmpath):
                names = pd.read_csv(cmpath).loc[:, v.univ_header_path]
                if np.array([os.path.isfile(n) for n in names]).all():
                    return True
            return False

        u.sout(f"Mode: Default, Orientation: {'Vertical' if orient == 'v' else 'Horizontal'}")
        u.sout(f"Running in '{v.location}'.")

        # Check for existing framework files
        if not filemap_exists():
            if not transforms_exist():
                if not mcalibs_exist():
                    exist = False
                    # Control mode
                    u.sout_header("File Map")
                    # Generate master calibration images
                    if args.dao:
                        dao_calib(orient)
                    else:
                        own_calib(orient)
                else:
                    exist = True
                    u.sout("! Master calibration images exist. Not redoing this.")
                get_transforms_manual(mcalibs_exist=exist)
            else:
                u.sout("! Transformations exist. Not redoing this.")

            build_filemap()
        else:
            u.sout("! File map exists. Not redoing this.")

        # Apply transformations to spectra
        u.sout_header("Transformation")
        apply_transforms(orient)

        # Output
        mem_current, mem_peak = tracemalloc.get_traced_memory()
        if args.silent:
            u.sout(
                f"{(time.default_timer() - t0) * 10**3:.2f} ms, "
                f"{mem_peak / 2**20:.2f} MiB RAM, "
                f"{v.storage_used / 2**20:.2f} MiB storage",
                force=True
            )
        else:
            u.sout_header("Finished")
            u.sout(f"Run duration: {(time.default_timer() - t0) * 10 ** 3:.2f} ms")
            u.sout(f"Max memory usage: {mem_peak / 2 ** 20:.2f} MiB")
            u.sout(f"Additional storage used: {v.storage_used / 2 ** 20:.2f} MiB")
            u.sout("")
        tracemalloc.stop()
    except KeyboardInterrupt:
        sys.exit("\nStopped.\n")


# HIGHLY recommended to keep THESE global names
global args, u, mh, v, L

if __name__ == "__main__":
    mpl.rcParams["image.interpolation"] = "none"
    mpl.rcParams["image.origin"] = "lower"
    main()
